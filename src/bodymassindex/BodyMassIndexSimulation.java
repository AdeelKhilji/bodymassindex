/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bodymassindex;

/**
 *
 * @author Adeel Khilji
 */
public class BodyMassIndexSimulation 
{
    public static void main(String[] args)
    {
        //Person person = new Person("Ram Persad", 82, Weight.Lb, 1,Height.Cm);
        //System.out.println(person.getHeightInMeters());
        Person person = new Person("Ram Persad", 82, Weight.Lb, 172,Height.Cm);
        String personBMI = "NAME: %s\nWEIGHT IN LBS:%.2f\nHEIGHT IN CENTIMETERS:%.0f\nBODY MASS INDEX:%.1f\n";
        System.out.printf(personBMI,person.getName(), person.getWeightLbs() , person.getHeightCm(), person.getBMI());
        System.out.println();
        person = new Person("Domenic Ip", 70, Weight.Lb, 152, Height.Cm);
        System.out.printf(personBMI,person.getName(), person.getWeightLbs() , person.getHeightCm(), person.getBMI());
        System.out.println();
        
    }
}
