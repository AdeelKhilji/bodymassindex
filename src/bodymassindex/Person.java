/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bodymassindex;

/**
 *
 * @author AdeelKhilji
 */
public class Person 
{
    private String name;
    private double weight, height;
    private Weight unitWeight;
    private Height unitHeight;
    
    protected Person(String name, double weight, Weight unitWeight, double height, Height unitHeight)
    {
        this.name = name;
        this.weight = weight;
        this.height = height;
        this.unitWeight = unitWeight;
        this.unitHeight = unitHeight;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public double getWeight()
    {
        return this.weight;
    }
   
    public double getWeightLbs()
    {
        return this.weight / 2.2;
    }
    public double getHeightCm()
    {
        return this.height; 
    }
    public double getHeightInMeters()
    {
        return this.height / 100;
    }
    public double getHeightInInches()
    {
        return this.height * 0.393701;
    }
    public double getBMI()
    {
        double weightInKilos = 0;
        double heightInCentimeters = 0;
        switch(unitWeight)
        {
            case Kg: weightInKilos = weight;
            break;
            case Lb: weightInKilos = weight * 0.4535;
            break;
        }
        switch(unitHeight)
        {
            case Cm: heightInCentimeters = height;
            break;
            case M: heightInCentimeters = height / 100;
            break;
            case In: heightInCentimeters = height * 0.393701;
            break;
        }
        
        return weightInKilos / (Math.pow(this.getHeightInMeters(), 2));
        
    }
}
